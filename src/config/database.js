import mongoose from "mongoose";
import dotenv from "dotenv";

dotenv.config();

const dbHost = process.env.DB_HOST || "localhost";
const dbPort = process.env.DB_PORT || "27017";
const dbName = process.env.DB_DATABASE || "market_place_app_db";
const dbUsername = process.env.DB_USERNAME;
const dbPassword = process.env.DB_PASSWORD;
const cluster = process.env.DB_CLUSTER;
const connectionString = cluster
  ? `mongodb+srv://${dbUsername}:${dbPassword}@${cluster}/${dbName}`
  : `mongodb://${dbUsername}:${dbPassword}@${dbHost}:${dbPort}/${dbName}`;

export default {
  connect: () =>
    mongoose.connect(connectionString, {
      useNewUrlParser: true,
      useFindAndModify: false,
      useUnifiedTopology: true,
      useCreateIndex: true,
    }),
  dbName,
  connectionString,
  connection: () => {
    if (mongoose.connection) {
      return mongoose.connection;
    }
    return this.connect();
  },
};
