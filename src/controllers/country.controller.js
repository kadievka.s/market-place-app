import codeInternalErrors from "../utils/constants/codeInternalErrors";
import ResponseUtil from "../utils/functions/response";
import {
  getCountriesService,
  getStatesByCountryService,
} from "../services/country.service";
import log4js from "log4js";

const logger = log4js.getLogger();
logger.level = process.env.LOGGER_LEVEL;

async function getCountries(req, res) {
  logger.info("[getCountries] INIT");
  try {
    const result = getCountriesService();
    return ResponseUtil.success(res, result);
  } catch (error) {
    logger.error("[getCountries] ERROR", error);
    const errorCode = error.code
      ? error.code
      : codeInternalErrors.DATA_NOT_FOUND;
    ResponseUtil.badRequest(res, errorCode, error.message);
  }
  logger.info("[getCountries] FINISH");
}

async function getStatesByCountry(req, res) {
  logger.info("[getStatesByCountry] INIT");
  try {
    const result = getStatesByCountryService(req.query.countryIsoCode);
    return ResponseUtil.success(res, result);
  } catch (error) {
    logger.error("[getStatesByCountry] ERROR", error);
    const errorCode = error.code
      ? error.code
      : codeInternalErrors.DATA_NOT_FOUND;
    ResponseUtil.badRequest(res, errorCode, error.message);
  }
  logger.info("[getStatesByCountry] FINISH");
}

export { getCountries, getStatesByCountry };
