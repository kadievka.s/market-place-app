import codeInternalErrors from "../utils/constants/codeInternalErrors";
import ResponseUtil from "../utils/functions/response";
import {
  getUserProfile,
  loginService,
  registerService,
  responseWithJWT,
} from "../services/user.service";
import log4js from "log4js";
import defaultConstants from "../utils/constants/default";

const logger = log4js.getLogger();
logger.level = process.env.LOGGER_LEVEL;

async function login(req, res) {
  logger.info("[login] INIT");
  try {
    const { email, password } = req.body;
    let response = await loginService(email, password);
    if (!response.valid) {
      return ResponseUtil.unauthorized(
        res,
        codeInternalErrors.UNAUTHORIZED,
        codeInternalErrors.UNAUTHORIZED_MESSAGE,
        {
          failedAttempt: response.failedAttempt,
          attemptsAllowed: defaultConstants.FAILED_ATTEMPT_QTY,
        }
      );
    }
    responseWithJWT(res, response.user);
  } catch (error) {
    logger.error("[login] ERROR", error);
    const errorCode = error.code
      ? error.code
      : codeInternalErrors.DATA_NOT_FOUND;
    ResponseUtil.badRequest(res, errorCode, error.message);
  }
  logger.info("[login] FINISH");
}

async function register(req, res) {
  logger.info("[register] INIT");
  try {
    const result = await registerService(req.body);
    return ResponseUtil.success(res, result);
  } catch (error) {
    logger.error("[register] ERROR", error);
    const errorCode = error.code
      ? error.code
      : codeInternalErrors.DATA_NOT_FOUND;
    ResponseUtil.badRequest(res, errorCode, error.message);
  }
  logger.info("[register] FINISH");
}

async function profile(req, res) {
  logger.info("[profile] INIT");
  try {
    const result = await getUserProfile(req.user.id);
    return ResponseUtil.success(res, result);
  } catch (error) {
    logger.error("[profile] ERROR", error);
    const errorCode = error.code
      ? error.code
      : codeInternalErrors.DATA_NOT_FOUND;
    ResponseUtil.badRequest(res, errorCode, error.message);
  }
  logger.info("[profile] FINISH");
}

export { login, profile, register };
