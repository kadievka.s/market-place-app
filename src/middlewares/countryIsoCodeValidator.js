import Joi from "joi";
import log4js from "log4js";
import ResponseUtil from "../utils/functions/response";
import errors from "../utils/constants/codeInternalErrors";

const logger = log4js.getLogger();
logger.level = process.env.LOGGER_LEVEL;

const countryIsoCodeValidator = (req, res, next) => {
  logger.info("[countryIsoCodeValidator] INIT");

  const data = req.query;

  const schema = Joi.object({
    countryIsoCode: Joi.string().required(),
  });

  const { error } = schema.validate(data);

  logger.info("[countryIsoCodeValidator] FINISH");
  error
    ? ResponseUtil.unprocessableEntity(
        res,
        errors.VALIDATION_FAILED,
        errors.VALIDATION_FAILED_MESSAGE,
        error.details[0].message
      )
    : next();
};

export default countryIsoCodeValidator;
