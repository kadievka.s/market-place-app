import log4js from "log4js";
import ResponseUtil from "../utils/functions/response";
import errors from "../utils/constants/codeInternalErrors";

const logger = log4js.getLogger();
logger.level = process.env.LOGGER_LEVEL;

const errorHandlingJWT = (err, req, res, next) => {
  logger.info("[errorHandlingJWT] INIT");
  logger.error(`[errorHandlingJWT] ${err}`);

  if (err.name === "UnauthorizedError") {
    ResponseUtil.unauthorized(
      res,
      errors.UNAUTHORIZED,
      errors.UNAUTHORIZED_MESSAGE
    );
  }

  logger.info("[errorHandlingJWT] FINISH");
};

export default errorHandlingJWT;
