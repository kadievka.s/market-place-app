import Joi from "joi";
import log4js from "log4js";
import ResponseUtil from "../utils/functions/response";
import errors from "../utils/constants/codeInternalErrors";

const logger = log4js.getLogger();
logger.level = process.env.LOGGER_LEVEL;

const loginValidator = (req, res, next) => {
  logger.info("[loginValidator] INIT");

  const data = req.body;

  const schema = Joi.object({
    email: Joi.string().email().required(),
    password: Joi.string().min(8).max(16).required(),
  });

  const { error } = schema.validate(data);

  logger.info("[loginValidator] FINISH");
  error
    ? ResponseUtil.unprocessableEntity(
        res,
        errors.VALIDATION_FAILED,
        errors.VALIDATION_FAILED_MESSAGE,
        error.details[0].message
      )
    : next();
};

export default loginValidator;
