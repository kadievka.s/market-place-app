import Joi from "joi";
import log4js from "log4js";
import ResponseUtil from "../utils/functions/response";
import errors from "../utils/constants/codeInternalErrors";
import joiPhone from "joi-phone-number";

const JoiPhone = Joi.extend(joiPhone);

const logger = log4js.getLogger();
logger.level = process.env.LOGGER_LEVEL;

const registerValidator = (req, res, next) => {
  logger.info("[registerValidator] INIT");

  const data = req.body;

  const schema = Joi.object({
    email: Joi.string().email().required(),
    confirmEmail: Joi.string().email().valid(Joi.ref("email")).required(),
    password: Joi.string().min(8).max(16).required(),
    confirmPassword: Joi.string()
      .min(8)
      .max(16)
      .valid(Joi.ref("password"))
      .required(),
    firstName: Joi.string().required(),
    lastName: Joi.string().required(),
    countryIsoCode: Joi.string().required(),
    stateName: Joi.string().optional().allow(""),
    defaultLangIsoCode: Joi.string().optional().allow(""),
    mobilePhone: JoiPhone.string().phoneNumber().optional(),
    phone: JoiPhone.string().phoneNumber().optional(),
  });

  const { error } = schema.validate(data);

  logger.info("[registerValidator] FINISH");
  error
    ? ResponseUtil.unprocessableEntity(
        res,
        errors.VALIDATION_FAILED,
        errors.VALIDATION_FAILED_MESSAGE,
        error.details[0].message
      )
    : next();
};

export default registerValidator;
