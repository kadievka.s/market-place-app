import mongoose from "mongoose";
import mongooseBcrypt from "mongoose-bcrypt";
import mongoosePaginate from "mongoose-paginate";
import mongooseDelete from "mongoose-delete";
import defaultConstants from "../utils/constants/default";

const userSchema = new mongoose.Schema(
  {
    email: {
      type: String,
      required: true,
      unique: true,
      index: true,
      lowercase: true,
      trim: true,
    },
    countryIsoCode: String,
    countryName: String,
    stateName: String,
    firstName: String,
    lastName: String,
    active: {
      type: Boolean,
      default: false,
    },
    defaultLangIsoCode: {
      type: String,
      default: defaultConstants.LANG_ISO_CODE,
    },
    mobilePhone: String,
    phone: String,
    registerToken: String,
    resetPasswordToken: String,
    failedAttempt: {
      type: Number,
      default: 0,
    },
  },
  {
    timestamps: true,
  }
);

userSchema.plugin(mongooseBcrypt);
userSchema.plugin(mongoosePaginate);
userSchema.plugin(mongooseDelete, { deletedAt: true });

const User = mongoose.model("User", userSchema);

export default User;
