import express from "express";
import {
  getCountries,
  getStatesByCountry,
} from "../controllers/country.controller";
import countryIsoCodeValidator from "../middlewares/countryIsoCodeValidator";

const router = express.Router();

/**
 * @swagger
 * /api/v1.0.0/general/countries:
 *   get:
 *     description: Get all countries consuming countryjs API.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returns an array of country objects in the field of data.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   description: Represents the response to the petition.
 *                   example: true
 *                 data:
 *                   type: array
 *                   description: Contains service information.
 *                   items:
 *                     type: object
 *                     properties:
 *                       name:
 *                         type: string
 *                         example: Afghanistan
 *                       callingCodes:
 *                         type: array
 *                         items:
 *                           type: string
 *                           example: 93
 *                       isoCode:
 *                         type: string
 *                         example: AF
 *                 message:
 *                   type: string
 *                   example: Request successful
 */
router.get("/countries", getCountries);

/**
 * @swagger
 * /api/v1.0.0/general/states/:
 *   get:
 *     description: Get all country states or provinces by ISO Code consuming the countryjs API.
 *     produces:
 *       - application/json
 *     parameters:
 *       - in: query
 *         name: countryIsoCode
 *         type: string
 *         description: isoCode of country requested.
 *         required: true
 *         default: VE
 *     responses:
 *       200:
 *         description: Returns an array of the state states or provinces.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   description: Represents the response to the petition.
 *                   example: true
 *                 data:
 *                   type: array
 *                   items:
 *                     type: string
 *                     example: Amazonas
 *                 message:
 *                   type: string
 *                   example: Request successful
 */
router.get("/states", countryIsoCodeValidator, getStatesByCountry);

export default router;
