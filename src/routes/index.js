import express from "express";
import generalRoutes from "./general.routes";
import userRoutes from "./user.routes";

const app = express();

app.use(
  `${process.env.APP_URI + process.env.APP_VERSION}/general`,
  generalRoutes
);
app.use(`${process.env.APP_URI + process.env.APP_VERSION}/user`, userRoutes);

export default app;
