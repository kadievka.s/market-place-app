import express from "express";
import { login, profile, register } from "../controllers/user.controller";
import loginValidator from "../middlewares/loginValidator";
import registerValidator from "../middlewares/registerValidator";
import jwtMiddleware from "express-jwt";
import secrets from "../config/secrets";

const router = express.Router();

/**
 * @swagger
 * /api/v1.0.0/user/register/:
 *   post:
 *     summary: Adds a new user.
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *                required: true
 *                example: morasalvaje@mail.com
 *              confirmEmail:
 *                type: string
 *                required: true
 *                example: morasalvaje@mail.com
 *              password:
 *                type: string
 *                required: true
 *                example: 123456789
 *              confirmPassword:
 *                type: string
 *                required: true
 *                example: 123456789
 *              firstName:
 *                type: string
 *                required: true
 *                example: Kadievka
 *              lastName:
 *                type: string
 *                required: true
 *                example: Salcedo
 *              countryIsoCode:
 *                type: string
 *                required: true
 *                example: VE
 *              countryName:
 *                type: string
 *                required: true
 *                example: Venezuela
 *              stateName:
 *                type: string
 *                example: Distrito Federal
 *              defaultLangIsoCode:
 *                type: string
 *                example: en
 *              mobilePhone:
 *                type: string
 *                example: +58-04242232389
 *              phone:
 *                type: string
 *                example: +58-02124222320
 *     responses:
 *       422:
 *         description: Returns success; false, status code 422, internal error code 422, "Invalid request data" message, and specifies where the error is, in this example email field.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   description: Represents the response to the petition.
 *                   example: false
 *                 code:
 *                   type: number
 *                   description: internal error code.
 *                   example: 422
 *                 message:
 *                   type: string
 *                   example: Invalid request data
 *                 error:
 *                   type: string
 *                   example: \"email\" is required
 *       400:
 *         description: Returns false success, status code 400, internal error code 43, and a "User already registered" message when the user email is taken.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   description: Represents the response to the petition.
 *                   example: false
 *                 code:
 *                   type: number
 *                   description: internal error code.
 *                   example: 43
 *                 message:
 *                   type: string
 *                   example: User already registered
 *       200:
 *         description: Returns a user object in the field of data.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   description: Represents the response to the petition.
 *                   example: true
 *                 data:
 *                   type: object
 *                   description: Contains service information.
 *                   properties:
 *                     email:
 *                       type: string
 *                       required: true
 *                       example: morasalvaje@mail.com
 *                     firstName:
 *                       type: string
 *                       required: true
 *                       example: Kadievka
 *                     lastName:
 *                       type: string
 *                       required: true
 *                       example: Salcedo
 *                     countryIsoCode:
 *                       type: string
 *                       required: true
 *                       example: VE
 *                     countryName:
 *                       type: string
 *                       required: true
 *                       example: Venezuela
 *                     stateName:
 *                       type: string
 *                       example: Distrito Federal
 *                     defaultLangIsoCode:
 *                       type: string
 *                       example: en
 *                     mobilePhone:
 *                       type: string
 *                       example: +58-04242232389
 *                     phone:
 *                       type: string
 *                       example: +58-02124222320
 *                 message:
 *                   type: string
 *                   example: Request successful
 */
router.post("/register", registerValidator, register);

/**
 * @swagger
 * /api/v1.0.0/user/login/:
 *   post:
 *     summary: Log in with user email and password.
 *     requestBody:
 *       content:
 *         application/json:
 *           schema:
 *            type: object
 *            properties:
 *              email:
 *                type: string
 *                required: true
 *                example: morasalvaje@mail.com
 *              password:
 *                type: string
 *                required: true
 *                example: 123456789
 *     responses:
 *       422:
 *         description: Returns success; false, status code 422, internal error code 422, "Invalid request data" message, and specifies where the error is, in this example email field.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   description: Represents the response to the petition.
 *                   example: false
 *                 code:
 *                   type: number
 *                   description: internal error code.
 *                   example: 422
 *                 message:
 *                   type: string
 *                   example: Invalid request data
 *                 error:
 *                   type: string
 *                   example: \"email\" is required
 *       401:
 *         description: Returns false success, status code 401, internal error code 401, and a "Unauthorized access", data with failedAttempt information if email or password are incorrect.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   description: Represents the response to the petition.
 *                   example: false
 *                 code:
 *                   type: number
 *                   description: internal error code.
 *                   example: 401
 *                 message:
 *                   type: string
 *                   example: Unauthorized access
 *                 data:
 *                   type: object
 *                   properties:
 *                     failedAttempt:
 *                       type: number
 *                       example: 0
 *                     attemptsAllowed:
 *                       type: number
 *                       example: 5
 *       200:
 *         description: Returns a user object in the field of data.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   description: Represents the response to the petition.
 *                   example: true
 *                 data:
 *                   type: object
 *                   description: Contains service information.
 *                   properties:
 *                     user:
 *                       type: object
 *                       properties:
 *                         email:
 *                           type: string
 *                           required: true
 *                           example: morasalvaje@mail.com
 *                         firstName:
 *                           type: string
 *                           required: true
 *                           example: Kadievka
 *                         lastName:
 *                           type: string
 *                           required: true
 *                           example: Salcedo
 *                         countryIsoCode:
 *                           type: string
 *                           required: true
 *                           example: VE
 *                         countryName:
 *                           type: string
 *                           required: true
 *                           example: Venezuela
 *                         stateName:
 *                           type: string
 *                           example: Distrito Federal
 *                         defaultLangIsoCode:
 *                           type: string
 *                           example: en
 *                         mobilePhone:
 *                           type: string
 *                           example: +58-04242232389
 *                         phone:
 *                           type: string
 *                           example: +58-02124222320
 *                     jwt:
 *                       type: string
 *                       example: eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjYwNTM1M2Y4NzY0ZTIyMGI0MDllMzQwMyIsImlhdCI6MTYyMDIzMzMzNCwiZXhwIjoxNjIwMjQ0MTM0fQ.Dugeb-AMBwstVA4awozJ-ZY41QNKJ--KJfT_6vzTajg
 *                 message:
 *                   type: string
 *                   example: Request successful
 */
router.post("/login", loginValidator, login);

/**
 * @swagger
 * /api/v1.0.0/user/profile/:
 *   get:
 *     security:
 *       - jwt: []
 *     description: Get all country states or provinces by ISO Code consuming the countryjs API.
 *     produces:
 *       - application/json
 *     responses:
 *       200:
 *         description: Returns an object with user profile information.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   description: Represents the response to the petition.
 *                   example: true
 *                 data:
 *                   type: object
 *                   properties:
 *                     email:
 *                       type: string
 *                       required: true
 *                       example: morasalvaje@mail.com
 *                     firstName:
 *                       type: string
 *                       required: true
 *                       example: Kadievka
 *                     lastName:
 *                       type: string
 *                       required: true
 *                       example: Salcedo
 *                     countryIsoCode:
 *                       type: string
 *                       required: true
 *                       example: VE
 *                     countryName:
 *                       type: string
 *                       required: true
 *                       example: Venezuela
 *                     stateName:
 *                       type: string
 *                       example: Distrito Federal
 *                     defaultLangIsoCode:
 *                       type: string
 *                       example: en
 *                     mobilePhone:
 *                       type: string
 *                       example: +58-04242232389
 *                     phone:
 *                       type: string
 *                       example: +58-02124222320
 *                 message:
 *                   type: string
 *                   example: Request successful
 *       401:
 *         description: Returns false success, status code 401, internal error code 401, and an "Unauthorized access", when Authorization JWT is incorrect.
 *         content:
 *           application/json:
 *             schema:
 *               type: object
 *               properties:
 *                 success:
 *                   type: boolean
 *                   description: Represents the response to the petition.
 *                   example: false
 *                 code:
 *                   type: number
 *                   description: internal error code.
 *                   example: 401
 *                 message:
 *                   type: string
 *                   example: Unauthorized access
 */
router.get("/profile", jwtMiddleware({ secret: secrets.jwtSecret }), profile);

export default router;
