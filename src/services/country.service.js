import defaultConstants from "../utils/constants/default";
import countryjs from "countryjs";
import log4js from "log4js";

const logger = log4js.getLogger();
logger.level = process.env.LOGGER_LEVEL;

function getCountriesService() {
  return countryjs
    .all()
    .map((country) => {
      return {
        name: country.name,
        callingCodes: country.callingCodes,
        isoCode: country.ISO.alpha2,
      };
    })
    .filter((country) => country.name && country.isoCode)
    .sort((a, b) => {
      return a.name - b.name;
    });
}

function getStatesByCountryService(countryIsoCode) {
  logger.debug(
    `[getStatesByCountryService] INPUT countryIsoCode: ${countryIsoCode}`
  );
  if (countryIsoCode) {
    return countryjs.states(countryIsoCode, defaultConstants.ISO_CODE_2);
  }
  return null;
}

function getCountryNameByIsoCode(isoCode) {
  return countryjs.name(isoCode, defaultConstants.ISO_CODE_2);
}

export {
  getCountriesService,
  getCountryNameByIsoCode,
  getStatesByCountryService,
};
