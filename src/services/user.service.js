import ResponseUtil from "../utils/functions/response";
import log4js from "log4js";
import jwt from "jsonwebtoken";
import secrets from "../config/secrets";
import User from "../models/User";
import defaultConstants from "../utils/constants/default";
import throwErrorMessage from "../utils/functions/throwErrorMessage";
import errors from "../utils/constants/codeInternalErrors";
import { getCountryNameByIsoCode } from "./country.service";

const logger = log4js.getLogger();
logger.level = process.env.LOGGER_LEVEL;

async function updateUser(id, user) {
  logger.debug(`[updateUser] UPDATING... `);
  return User.findByIdAndUpdate(id, { $set: user }, { new: true }).select(
    "-password -registerToken -resetPasswordToken"
  );
}

async function serviceGetUserByEmail(email) {
  if (email) {
    return User.findOne({ email: email }).select("-resetPasswordToken");
  }
  return null;
}

async function getUserById(id) {
  if (id) {
    return User.findById(id).select(
      "-password -resetPasswordToken -registerToken"
    );
  }
  return null;
}

async function createUser(user) {
  if (user) {
    return await User.create(user);
  }
  return null;
}

async function loginService(email, password) {
  logger.debug(`[loginService] INIT`);
  let user = await serviceGetUserByEmail(email.toLowerCase());
  let valid = false;
  let failedAttempt = 0;
  if (user) {
    valid = await validatePassword(user, password);
    logger.debug(`[loginService] ${password} ${valid}`);
    if (valid && user.failedAttempt <= defaultConstants.FAILED_ATTEMPT_QTY) {
      failedAttempt = 0;
    } else {
      failedAttempt = ++user.failedAttempt;
      valid = false;
    }
    user = await updateUser(user._id, { failedAttempt });
  }
  logger.debug(`[loginService] FINISHED`);
  return { user, failedAttempt, valid };
}

async function validatePassword(user, password) {
  logger.debug(`[validatePassword] USER: ${JSON.stringify(user)}`);
  if (user) {
    logger.debug(
      `[validatePassword] USER: ${user.email} ${user.password} ${password}`
    );
    return user.verifyPassword(password);
  }

  return false;
}

function responseWithJWT(response, user) {
  logger.info(`[responseWithJWT] INIT`);
  const token = jwt.sign(
    {
      id: user._id,
    },
    secrets.jwtSecret,
    {
      expiresIn: 60 * 180,
    }
  );
  logger.info(`[responseWithJWT] FINISH`);
  return ResponseUtil.success(response, {
    user: getUserFieldsToResponse(user),
    jwt: token,
  });
}

async function registerService(reqBody) {
  logger.debug(`[registerService] INIT`);
  let user = await serviceGetUserByEmail(reqBody.email.toLowerCase());
  if (user) {
    throwErrorMessage(
      errors.USER_ALREADY_REGISTERED_MESSAGE,
      errors.USER_ALREADY_REGISTERED
    );
  }
  reqBody.active = true;
  reqBody.countryName = getCountryNameByIsoCode(reqBody.countryIsoCode);
  if (!reqBody.defaultLangIsoCode)
    reqBody.defaultLangIsoCode = defaultConstants.LANG_ISO_CODE;
  user = getUserFieldsToResponse(await createUser(reqBody));
  logger.debug(`[registerService] FINISHED`);
  return user;
}

function getUserFieldsToResponse(userModel) {
  return {
    email: userModel.email,
    firstName: userModel.firstName,
    lastName: userModel.lastName,
    countryIsoCode: userModel.countryIsoCode,
    countryName: userModel.countryName,
    stateName: userModel.stateName,
    defaultLangIsoCode: userModel.defaultLangIsoCode,
    mobilePhone: userModel.mobilePhone,
    phone: userModel.phone,
  };
}

async function getUserProfile(userId) {
  const user = await getUserById(userId);
  if (!user) {
    throwErrorMessage(errors.USER_NOT_FOUND_MESSAGE, errors.USER_NOT_FOUND);
  }
  return getUserFieldsToResponse(user);
}

export {
  getUserById,
  getUserProfile,
  loginService,
  registerService,
  responseWithJWT,
  serviceGetUserByEmail,
  updateUser,
  validatePassword,
};
