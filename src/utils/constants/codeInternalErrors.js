const codeInternalErrors = {
  USER_NOT_FOUND: 40,
  USER_NOT_FOUND_MESSAGE: "User not found",

  USER_EXISTS: 41,
  USER_EXISTS_MESSAGE: "User already defined",

  EMAIL_REQUIRED: 42,
  EMAIL_REQUIRED_MESSAGE: "Email no found",

  USER_ALREADY_REGISTERED: 43,
  USER_ALREADY_REGISTERED_MESSAGE: "User already registered",

  DATA_NOT_FOUND: 50,
  DATA_NOT_FOUND_MESSAGE: "Data not found",

  INSUFFICIENT_DATA: 51,
  INSUFFICIENT_DATA_MESSAGE: "The data provided is insufficient",

  BAD_REQUEST: 52,
  BAD_REQUEST_MESSAGE: "The data provided is poorly structured",

  IMAGE_NOT_SAVED: 53,
  IMAGE_NOT_SAVED_MESSAGE: "Image not saved, retry again",

  PASSWORD_NO_FOUND: 54,
  PASSWORD_NO_FOUND_MESSAGE: "Password no found",

  PROCESS_NOT_FINISHED: 55,
  PROCESS_NOT_FINISHED_MESSAGE: "Process not finished",

  PARAM_NO_FOUND: 80,
  PARAM_NO_FOUND_MESSAGE: "Param query no found",

  IMAGE_NO_FOUND: 90,
  IMAGE_NO_FOUND_MESSAGE: "Image not found",

  BODY_DATA_REQUIRED: 100,

  UNAUTHORIZED: 401,
  UNAUTHORIZED_MESSAGE: "Unauthorized access",

  FORBIDDEN: 403,
  FORBIDDEN_MESSAGE: "You do not have permissions to perform this operation",

  USER_NOT_REGISTERED: "User is not registered",
  USER_REGISTERED_INCOMPLETE: "User register is incomplete",

  VALIDATION_FAILED: 422,
  VALIDATION_FAILED_MESSAGE: "Invalid request data",

  CONFLICT: 409,
  NOT_ENOUGH_STOCK_TO_SATISFY: "Not enough stock to satisfy",
};

export default codeInternalErrors;
