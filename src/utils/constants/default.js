const defaultConstants = {
  LANG_ISO_CODE: "en",
  LANG_ISO_CODE_ES: "es",
  CONTENT_TYPE: "application/json",
  DEFAULT_LIMIT_PER_PAGE: 5,
  OPTION_ASC: "asc",
  OPTION_DESC: "desc",
  ISO_CODE_2: "ISO2",
  FAILED_ATTEMPT_QTY: 5,
};

export default defaultConstants;
