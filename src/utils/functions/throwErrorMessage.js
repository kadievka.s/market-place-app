import errors from "../constants/codeInternalErrors";

export default function throwErrorMessage(
  message = errors.VALIDATION_FAILED_MESSAGE,
  code = errors.VALIDATION_FAILED
) {
  let error = new Error(message);
  error.code = code;
  throw error;
}
